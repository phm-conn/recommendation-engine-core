import type {
  MicroUID,
  Story,
  StoryDump,
  User,
} from "https://gitlab.com/phm-conn/recommendation-engine-bundeler/-/raw/v0.0.8/mod.ts";
export { MicroUID, Story, StoryDump, User };
export type StripableInternals<FullStory> = {
  _key?: string;
  _id?: string;
  _rev?: string;
};
export interface StoryExport extends MicroUID {
  details: Story;
  authors: Array<User>;
  faved: Array<User>;
}
