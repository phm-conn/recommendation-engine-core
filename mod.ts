import {
  Application,
  Context,
  Router,
} from "https://deno.land/x/oak@v10.5.1/mod.ts";
import { RecEngineStore } from "https://gitlab.com/phm-conn/recommendation-engine-bundeler/-/raw/v0.0.8/mod.ts";
import type {
  Domains,
  FullStory,
  Story,
} from "https://gitlab.com/phm-conn/recommendation-engine-bundeler/-/raw/v0.0.8/mod.ts";
import { auth, database, host } from "./auth.ts";
import type { StoryExport, StripableInternals } from "./get-types.ts";
import { WsLoop } from "./ws-loop.ts";
import { Semaphore } from "https://deno.land/x/semaphore@v1.1.0/mod.ts";

const app = new Application();
const store = await RecEngineStore.FastSetup(host, database, auth);
const loop = new WsLoop(store);
loop.setup();
const publicQueue = new Semaphore(2);

const router = new Router();
router.get("/flood", (ctx) => {
  ctx.response.headers.append("Access-Control-Allow-Origin", "*");
  loop.wsLoop(ctx.upgrade(), publicQueue);
});
const invalidEndpoint = (ctx: Context) => {
  ctx.response.status = 300;
  ctx.response.body = JSON.stringify({
    code: 300,
    reason: "Invalid Endpoint Request",
  });
  console.log("Invalid Endpoint Request");
};

// BEGIN ALPHA API
router.get("/alpha", invalidEndpoint);

// BEGIN BOOK DUMPER
router.get("/alpha/book", invalidEndpoint);
router.get("/alpha/book/:domain", invalidEndpoint);
router.get("/", invalidEndpoint);
router.get("/alpha/book/:domain/:id", async (ctx) => {
  const domain = ctx?.params?.domain;
  const id = parseInt(ctx?.params?.id);
  if (isNaN(id)) {
    ctx.response.status = 400;
    ctx.response.body = JSON.stringify({ code: 400, reason: "Invalid id" });
  }
  try {
    const grab = await store.dumpStory(domain as Domains, id);
    if (!grab) {
      ctx.response.status = 300;
      ctx.response.body = JSON.stringify({
        code: 404,
        reason: "Profile Not Cached",
      });
      return;
    }
    ctx.response.status = 200;
    const stripping: StripableInternals<FullStory> = grab.details as FullStory;
    delete stripping._key;
    delete stripping._id;
    delete stripping._rev;
    const story = stripping as Story;
    const res: StoryExport = {
      ...grab,
      details: story,
    };
    ctx.response.body = JSON.stringify(res);
  } catch (e) {
    const err = e as Error;
    ctx.response.status = 400;
    ctx.response.body = JSON.stringify({
      code: 400,
      reason: err?.message || "Unknown Error",
    });
  }
});

// BEGIN USER DUMPER
router.get("/alpha/user", invalidEndpoint);
router.get("/alpha/user/:domain", invalidEndpoint);
router.get("/", invalidEndpoint);
router.get("/alpha/user/:domain/:id", async (ctx) => {
  const domain = ctx?.params?.domain;
  const id = parseInt(ctx?.params?.id);
  if (isNaN(id)) {
    ctx.response.status = 400;
    ctx.response.body = JSON.stringify({ code: 400, reason: "Invalid id" });
  }
  try {
    const grab = await store.dumpUser(domain as Domains, id);
    if (!grab) {
      ctx.response.status = 300;
      ctx.response.body = JSON.stringify({
        code: 404,
        reason: "User Not Cached",
      });
      return;
    }
    ctx.response.status = 200;
    ctx.response.body = JSON.stringify(grab);
  } catch (e) {
    const err = e as Error;
    ctx.response.status = 400;
    ctx.response.body = JSON.stringify({
      code: 400,
      reason: err?.message || "Unknown Error",
    });
  }
});

app.use(router.routes());
app.use(router.allowedMethods());

const serv = app.listen({ port: 8000 });
console.log(`Started Server on port ${8000}`);
await serv;
