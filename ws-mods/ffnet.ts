import type {
  RecEngineStore,
  ReviewGroup,
  Story,
  User,
} from "https://gitlab.com/phm-conn/recommendation-engine-bundeler/-/raw/v0.0.8/mod.ts";
import { AsyncWebSocket } from "https://gitlab.com/phm-conn/async-websockets/-/raw/v0.0.1/mod.ts";
import { FictionPressParser } from "https://gitlab.com/phm-conn/ffnet-fpcom-scraper-api/-/raw/v0.0.8/mod.ts";
import { Semaphore } from "https://deno.land/x/semaphore@v1.1.0/mod.ts";
import { Mod as ModSup, Params, ResolvableError } from "./../ws-params.ts";
import { delay } from "https://deno.land/std@0.136.0/async/mod.ts";
import { useragent } from "./../auth.ts";

export class Mod extends ModSup {
  public static modName = "ffnet";
  private parser?: FictionPressParser;
  constructor(
    store: RecEngineStore,
  ) {
    super(store);
  }
  async setup() {
    this.parser = new FictionPressParser(useragent, "www.fanfiction.net");
    await this.parser.setup();
  }
  valid(params: Params) {
    if (params.domain == "ffnet") {
      return true;
    }
    return false;
  }
  async middleware(
    params: Params,
    socket: AsyncWebSocket,
    semaphore: Semaphore,
  ) {
    try {
      if (params.mode == "story") {
        if (isNaN(params.idn)) {
          socket.send(`${params.retid}|error|invalid id number`);
          return;
        }
        this.parseReviewsWrapper(socket, params, semaphore);
        return;
      }
      if (params.mode == "user") {
        if (isNaN(params.idn)) {
          socket.send(`${params.retid}|error|invalid user number`);
          return;
        }
        await this.parseUserWrapper(socket, params, semaphore);
        return;
      }
      socket.send(`${params.retid}|error|invalid mode`);
    } catch (e) {
      if (e instanceof ResolvableError) {
        socket.send(`${params.retid}|error|${e.message}`);
      } else {
        throw e;
      }
    }
    return;
  }
  //TODO: Convert all to yield so status updates can be sent.
  private async parseUserWrapper(
    socket: AsyncWebSocket,
    params: Params,
    semaphore: Semaphore,
  ) {
    if (isNaN(params.idn)) {
      socket.send(`invalid id number`);
      return;
    }
    const found = await this.store.dumpUser(
      params.domain,
      params.idn,
    );
    //TODO: Add cache shorting logic. (90m for direct requests?)
    if (found && false) {
      socket.send(
        `${params.retid}|found|/alpha/user/${params.domain}/${params.idn}`,
      );
      return;
    }
    socket.send(
      `${params.retid}|queued`,
    );
    const release = await semaphore.acquire();
    socket.send(
      `${params.retid}|fetching`,
    );
    const ret = await this.parseFFNetProfile(params.idn);
    if (ret instanceof ResolvableError) {
      socket.send(`${params.retid}|error|${ret.message}`);
      await release();
      return;
    }
    socket.send(
      `${params.retid}|cached|/alpha/user/${params.domain}/${params.idn}`,
    );
    await release();
  }
  private async parseFFNetProfileOrCached(id: number) {
    const found = await this.store.dumpUser(
      "ffnet",
      id,
    );
    const last = found?.lastScrape || 0;
    //                             ms     s    m    h   d
    const weekAgo = Date.now() - (1000 * 60 * 60 * 24 * 7);
    if (last >= weekAgo) {
      return;
    }
  }
  private async parseFFNetProfile(id: number) {
    if (!this.parser) {
      return new ResolvableError("FFNet Parser not setup?");
    }
    const info = await this.parser.profile(id);
    const user: User = {
      domain: "ffnet",
      id: id,
      name: info.name,
      lastScrape: Date.now(),
    };
    const topAdded = await this.store.addUser(user);
    let added = 0;
    for (const fave of info.favStories) {
      const story: Story = {
        domain: "ffnet",
        id: fave.id,
        name: fave.title,
        lastScrape: undefined,
        wordcount: fave.wordcount,
        leadcount: undefined,
        datesubmit: fave.datesubmit,
        dateupdate: fave.dateupdate,
        chapters: fave.chapters,
        statusid: fave.statusid,
        blurb: fave.blurb,
        fandoms: [fave.category],
        tags: fave.tags,
      };
      const addedStory = await this.store.addStory(story);
      //console.log(addedStory.name, story.name, addedStory._key);
      const user: User = {
        domain: "ffnet",
        id: fave.authorID,
        name: fave.authorName,
        lastScrape: undefined,
      };
      const addedUser = await this.store.addUser(user);
      await this.store.addFave({
        _from: topAdded._id,
        _to: addedStory._id,
      });
      await this.store.addAuthor({
        _from: addedUser._id,
        _to: addedStory._id,
      });
      added++;
    }
    console.log(`Added ${added} faves for ${info.name}`);
    added = 0;
    for (const authored of info.myStories) {
      const story: Story = {
        domain: "ffnet",
        id: authored.id,
        name: authored.title,
        lastScrape: undefined,
        wordcount: authored.wordcount,
        leadcount: undefined,
        datesubmit: authored.datesubmit,
        dateupdate: authored.dateupdate,
        chapters: authored.chapters,
        statusid: authored.statusid,
        blurb: authored.blurb,
        fandoms: [authored.category],
        tags: authored.tags,
      };
      const addedStory = await this.store.addStory(story);
      await this.store.addAuthor({
        _from: topAdded._id,
        _to: addedStory._id,
      });
      added++;
    }
    console.log(`Added ${added} authored works for ${info.name}`);
  }
  //TODO: Add cache shorting logic. (3d for requests?)
  private async parseReviewsWrapper(
    socket: AsyncWebSocket,
    params: Params,
    semaphore: Semaphore,
  ) {
    if (isNaN(params.idn)) {
      socket.send(`invalid id number`);
      return;
    }
    const found = await this.store.dumpStory(
      params.domain,
      params.idn,
    );
    if (found && false) {
      socket.send(
        `${params.retid}|found|/alpha/book/${params.domain}/${params.idn}`,
      );
      return;
    }
    socket.send(
      `${params.retid}|queued`,
    );
    const release = await semaphore.acquire();
    socket.send(
      `${params.retid}|fetching|loading reviews`,
    );
    const count = await this.parseFFNetReviews(params.idn);
    if (count instanceof ResolvableError) {
      socket.send(`${params.retid}|error|${count.message}`);
      await release();
      return;
    }
    socket.send(
      `${params.retid}|fetching|following ${count} leads`,
    );
    const ret = await this.followLeads(params.idn, count);
    if (ret instanceof ResolvableError) {
      socket.send(`${params.retid}|error|${ret.message}`);
      await release();
      return;
    }
    socket.send(
      `${params.retid}|cached|/alpha/book/${params.domain}/${params.idn}`,
    );
    await release();
  }
  private async parseFFNetReviews(id: number) {
    if (!this.parser) {
      return new ResolvableError("FFNet Parser not setup?");
    }
    const story = await this.store.storyById("ffnet", id);
    if (!story) {
      return new ResolvableError("Story has no source?");
    }
    const info = await this.parser.reviews(id);
    story.leadcount = info.loadedPages;
    let added = 0;
    const grouped: Map<number, { user: User; reviews: ReviewGroup }> = new Map<
      number,
      { user: User; reviews: ReviewGroup }
    >();
    for (const review of info.reviews) {
      let user: User | undefined = grouped.get(review.userID)?.user;
      const reviewGroup: ReviewGroup = grouped.get(review.userID)?.reviews ||
        {};
      if (!user) {
        user = {
          domain: "ffnet",
          id: review.userID,
          name: review.username,
        };
      }
      reviewGroup[review.time + ""] = {
        time: review.time,
        review: review.review,
      };
      grouped.set(review.userID, { user, reviews: reviewGroup });
      added++;
    }
    console.log(`${added} reviews processed`);
    added = 0;
    for (const group of grouped) {
      const user = await this.store.addUser(group[1].user);
      this.store.addReview({
        _from: story._id,
        _to: user._id,
        reviews: group[1].reviews,
      });
      added++;
    }
    console.log(`${added} users added as leads`);
    return added;
  }
  private async followLeads(id: number, total: number) {
    const leads = await this.store.dumpLeads("ffnet", id);
    if (!leads) {
      return new ResolvableError("Didn't find any leads :(");
    }
    let followed = 0;
    for (const lead of leads.reviewed) {
      try {
        await this.parseFFNetProfileOrCached(lead.id);
      } catch (e) {
        console.warn(`error on ${lead.domain}/${lead.id}`);
        console.warn(e);
        await delay(5000);
      }
      await delay(250);
      followed++;
      console.log(`${followed}/${total}`);
    }
    console.log(`Followed ${followed} leads.`);
  }
}
