import type { RecEngineStore } from "https://gitlab.com/phm-conn/recommendation-engine-bundeler/-/raw/v0.0.8/mod.ts";
import { AsyncWebSocket } from "https://gitlab.com/phm-conn/async-websockets/-/raw/v0.0.1/mod.ts";
import { FictionPressParser } from "https://gitlab.com/phm-conn/ffnet-fpcom-scraper-api/-/raw/v0.0.5/mod.ts";
import { Semaphore } from "https://deno.land/x/semaphore@v1.1.0/mod.ts";
export type Parsers = { ffnet: FictionPressParser };
import type { Params } from "./ws-params.ts";
import { Mod } from "./ws-params.ts";

const messageParser =
  /(?<rid>[^\|]+)\|(?<mode>[^\|]+)\|(?<domain>[^\|]+)\|(?<id>[^\|]+)/;

export class WsLoop {
  private store: RecEngineStore;
  private mods: Array<Mod> = new Array<Mod>();
  constructor(store: RecEngineStore) {
    this.store = store;
  }

  public async setup() {
    const imps = [];
    for await (const dirEntry of Deno.readDir("./ws-mods")) {
      if (!dirEntry.isFile) {
        continue;
      }
      imps.push(import(`./ws-mods/${dirEntry.name}`));
    }
    const newMods = await Promise.all(imps);
    for (const newMod of newMods) {
      const mod = new newMod.Mod(this.store) as Mod;
      await mod.setup();
      this.mods.push(mod);
      console.log(`Setup wsMod ${newMod.Mod.modName}`);
    }
  }

  private static parser(
    message: string,
  ): Params | false {
    const match = message.match(messageParser)?.groups;
    const retid = match?.rid;
    const mode = match?.mode;
    const domain = match?.domain;
    const id = match?.id;
    const idn = parseInt(id + "");
    if (!retid || !mode || !domain || !id) {
      return false;
    }
    return { retid, mode, domain, id, idn };
  }

  async wsLoop(
    nativeSocket: WebSocket,
    semaphore: Semaphore,
  ) {
    const socket = new AsyncWebSocket(nativeSocket);
    await socket.holdTilOpen();
    try {
      socket.send("0|connected|ready");
      for await (const message of socket.holdForMessages()) {
        const payload = message.data;
        if (typeof payload != "string") {
          socket.send(`invalid command type`);
          continue;
        }
        const params = WsLoop.parser(payload);
        if (!params) {
          socket.send(`invalid command format`);
          console.log(payload);
          continue;
        }
        try {
          let found = false;
          for (const mod of this.mods) {
            found = found || await mod.valid(params);
            if (found) {
              const _p = mod.middleware(params, socket, semaphore);
              break;
            }
          }
          if (!found) {
            socket.send(`${params.retid}|error|invalid domain`);
          }
          continue;
        } catch (e) {
          if (e instanceof Error) {
            socket.send(`${params.retid}|error|${e.message}`);
            continue;
          }
          socket.send(`${params.retid}|error|Unknown Error`);
          continue;
        }
      }
    } catch (e) {
      let skip = false;
      if (e instanceof DOMException) {
        if (e.name == "AbortError") {
          skip = true;
        }
      }
      if (!skip) {
        console.log(e);
      }
    }
    const close = await socket.holdTilClosed();
    console.log(close);
  }
}
