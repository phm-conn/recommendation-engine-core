import type {
  RecEngineStore,
} from "https://gitlab.com/phm-conn/recommendation-engine-bundeler/-/raw/v0.0.8/mod.ts";
import type { Semaphore } from "https://deno.land/x/semaphore@v1.1.0/mod.ts";
import type { AsyncWebSocket } from "https://gitlab.com/phm-conn/async-websockets/-/raw/v0.0.1/mod.ts";
export type Params = {
  retid: string;
  mode: string;
  domain: string;
  id: string;
  idn: number;
};
export class ResolvableError extends Error {
  public resolvable = true;
}
export abstract class Mod {
  public static modName = "Abstract";
  protected store: RecEngineStore;
  constructor(
    store: RecEngineStore,
  ) {
    this.store = store;
  }
  public abstract setup(): Promise<void>;
  public abstract valid(params: Params): boolean;
  public abstract middleware(
    params: Params,
    socket: AsyncWebSocket,
    sema: Semaphore,
  ): Promise<void>;
}
