import type { ArangoAuthentication } from "https://gitlab.com/phm-conn/recommendation-engine-bundeler/-/raw/v0.0.3/mod.ts";
export const auth: ArangoAuthentication = {
  username: "testing",
  password: "test",
};
export const host = "localhost";
export const database = "testing";
export const useragent = "testuseragent";
